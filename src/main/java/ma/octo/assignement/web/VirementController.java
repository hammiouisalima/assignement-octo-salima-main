package ma.octo.assignement.web;


import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VirementServiceIn;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@RestController
@RequestMapping("virements")
class VirementController {
	
	    @Autowired
	    private VirementServiceIn virementService;

	    @GetMapping("")
	    List<VirementDto> loadAll() {
	        return virementService.loadAll();
	    }

	    @PostMapping("")
	    @ResponseStatus(HttpStatus.CREATED)
	    public void createTransaction(@RequestBody VirementDto virementDto)
	            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

	        virementService.saveVirementDto(virementDto);
	    }
	
}
