package ma.octo.assignement.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import ma.octo.assignement.domain.AuditOperationBanque;

public interface AuditOperationRepository extends JpaRepository<AuditOperationBanque, Long> {
}
