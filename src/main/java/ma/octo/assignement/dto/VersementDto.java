package ma.octo.assignement.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ma.octo.assignement.domain.Compte;

@Data
@NoArgsConstructor
@ToString
public class VersementDto implements Serializable {
	
	private Compte CompteBeneficiaire;
    private BigDecimal montantV;
    private Date dateO;
    private String CINEmetteur;
}
