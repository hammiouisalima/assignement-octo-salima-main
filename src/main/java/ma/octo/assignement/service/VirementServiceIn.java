package ma.octo.assignement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

@Service
public interface VirementServiceIn {
	 List<VirementDto> loadAll();
	    void saveVirement(Virement virement) throws CompteNonExistantException, TransactionException;
	    void saveVirementDto(VirementDto virementDto) throws CompteNonExistantException, TransactionException;

}
