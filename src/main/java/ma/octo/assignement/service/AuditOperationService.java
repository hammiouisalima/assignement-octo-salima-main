package ma.octo.assignement.service;


import ma.octo.assignement.domain.AuditOperationBanque;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditOperationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class AuditOperationService {

    Logger LOGGER = LoggerFactory.getLogger(AuditOperationService.class);

    @Autowired
    private AuditOperationRepository auditOperationRepository;



    public void auditVersement(String message) {

        LOGGER.info("Audit de l'événement {}", EventType.VERSEMENT);

        AuditOperationBanque audit = new AuditOperationBanque();
        audit.setEventType(EventType.VERSEMENT);
        audit.setMessage(message);
        auditOperationRepository.save(audit);
    }


	public void auditVirement(String message) {
		// TODO Auto-generated method stub
		LOGGER.info("Audit de l'événement {}", EventType.VIREMENT);

		AuditOperationBanque audit = new AuditOperationBanque();
        audit.setEventType(EventType.VIREMENT);
        audit.setMessage(message);
        auditOperationRepository.save(audit);
		
	}
}
