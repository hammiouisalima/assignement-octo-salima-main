package ma.octo.assignement.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VirementRepository;
import ma.octo.assignement.service.AuditOperationService;
import ma.octo.assignement.service.CompteServiceIn;
import ma.octo.assignement.service.VersementServiceIn;

@Service
public class VersementService implements VersementServiceIn{

	public static final BigDecimal MONTANT_MAXIMAL = new BigDecimal(1000);
    Logger LOGGER = LoggerFactory.getLogger(AuditOperationService.class);


    @Autowired
    private VersementRepository versementRepository;
    @Autowired
    private CompteServiceIn compteService;
    @Autowired
    private AuditOperationService auditService;


    @Override
    public List<VersementDto> loadAll() {
        List<VersementDto> versementDtoList = new ArrayList<>();
        List<Versement> versements = versementRepository.findAll();
        if (!CollectionUtils.isEmpty(versements)) {
            for (int i = 0; i < versements.size(); i++){
                versementDtoList.add(VersementMapper.map(versements.get(i)));
            }
        }
        return versementDtoList;
    }

    @Override
    public void saveVersement(Versement versement) {
        versementRepository.save(versement);
    }

    @Override
    public void saveVersementDto(VersementDto versementDto) throws CompteNonExistantException, TransactionException {
        Compte compteBeneficiaire = compteService.findByRib(versementDto.getCompteBeneficiaire().getRib());
        BigDecimal big10 = new BigDecimal(10);
        versementDto.setDateO(new Date());
        if (compteBeneficiaire == null) {
            System.out.println("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }

        if (versementDto.getMontantV().equals(BigDecimal.ZERO) ) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantV().equals(BigDecimal.ZERO)) {
            System.out.println("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantV().compareTo(big10) < 0) {
            System.out.println("Montant minimal de Versement non atteint");
            throw new TransactionException("Montant minimal de Versement non atteint");
        } else if (versementDto.getMontantV().compareTo(MONTANT_MAXIMAL) > 0) {
            System.out.println("Montant maximal de Versement dépassé");
            throw new TransactionException("Montant maximal de Versement dépassé");
        }

        compteBeneficiaire.setSolde((BigDecimal)compteBeneficiaire.getSolde().add(versementDto.getMontantV()));
        compteService.saveCompte(compteBeneficiaire);

        Versement versement = new Versement();
        versement.setDateOperation(versementDto.getDateO());
        versement.setCompteBeneficiaire(compteBeneficiaire);
        versement.setMontantVirement(versementDto.getMontantV());

        versementRepository.save(versement);


        auditService.auditVersement("Le versement d'un montant de " + versementDto.getMontantV()+ " vers le compte " + versementDto
                .getCompteBeneficiaire().getRib());
    }
    
}
