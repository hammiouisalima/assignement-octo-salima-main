package ma.octo.assignement.service;

import java.util.List;

import org.springframework.stereotype.Service;

import ma.octo.assignement.domain.Utilisateur;


public interface UtilisateurServiceIn {
	 List<Utilisateur> loadAll();
	 void saveUtilisateur(Utilisateur utilisateur);
}
