package ma.octo.assignement.service;

import java.util.List;
import org.springframework.stereotype.Service;
import ma.octo.assignement.domain.Compte;


@Service
public interface CompteServiceIn {
	List<Compte> loadAll();
    void saveCompte(Compte compte);
    Compte getCompte(Long id);
    Compte findByNrCompte(String numeroCompte);
    Compte findByRib(String rib);
    void deleteCompte(Long id);
    
    
	
}
