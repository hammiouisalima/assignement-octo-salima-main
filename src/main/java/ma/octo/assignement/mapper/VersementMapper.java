package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDto;

public class VersementMapper {
	
	public static VersementDto map(Versement versement) {
        VersementDto versementDto = new VersementDto();

        versementDto.setDateO(versement.getDateOperation());
        versementDto.setCompteBeneficiaire(versement.getCompteBeneficiaire());
        versementDto.setMontantV(versement.getMontantVirement());
        versementDto.setCINEmetteur(versement.getCINEmetteur());

        return versementDto;
    }

    public static Versement map(VersementDto versementDto) {
        Versement versement = new Versement();

        versement.setDateOperation(versementDto.getDateO());
        versement.setCompteBeneficiaire(versementDto.getCompteBeneficiaire());
        versement.setMontantVirement(versementDto.getMontantV());
        versement.setCINEmetteur(versementDto.getCINEmetteur());

        return versement;
    }

}
