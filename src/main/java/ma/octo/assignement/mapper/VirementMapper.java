package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.dto.VirementDto;

public class VirementMapper {

    //private static VirementDto virementDto;
   //
    public static VirementDto map(Virement virement) {
        VirementDto virementDto = new VirementDto();
        virementDto.setCompteEmetteur(virement.getCompteEmetteur());
        virementDto.setCompteBeneficiaire(virement.getCompteBeneficiaire());
        virementDto.setDateOperation(virement.getDateOperation());
        virementDto.setMotifVirement(virement.getMotifVirement());
        virementDto.setMontantVirement(virement.getMontantVirement());

        return virementDto;

    }
    public  Virement map(VirementDto virementDto) {

        Virement virement = new Virement();
        virement.setCompteEmetteur(virementDto.getCompteEmetteur());
        virement.setCompteBeneficiaire(virementDto.getCompteBeneficiaire());
        virement.setDateOperation(virementDto.getDateOperation());
        virement.setMotifVirement(virementDto.getMotifVirement());
        virement.setMontantVirement(virementDto.getMontantVirement());

        return virement;
    }
}
