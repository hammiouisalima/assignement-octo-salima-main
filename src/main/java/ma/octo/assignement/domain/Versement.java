package ma.octo.assignement.domain;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "VERSEMENT")
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@DiscriminatorValue("VERSEMENT")
public class Versement extends OperationBanque{

    @Id  @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String CINEmetteur;
}
