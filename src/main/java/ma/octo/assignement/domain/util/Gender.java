package ma.octo.assignement.domain.util;

public enum Gender {
	F("Femme"),
	H("Homme");
	
	private String gender;

	private Gender(String gender) {
		this.gender = gender;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}
	
	
}
