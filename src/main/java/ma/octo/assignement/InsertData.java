package ma.octo.assignement;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.service.CompteServiceIn;
import ma.octo.assignement.service.UtilisateurServiceIn;
import ma.octo.assignement.service.VersementServiceIn;
import ma.octo.assignement.service.VirementServiceIn;


@Component
public class InsertData implements CommandLineRunner {

	
	@Autowired
    private CompteServiceIn compteService;
    @Autowired
    private UtilisateurServiceIn utilisateurService;
    @Autowired
    private VirementServiceIn virementService;
    @Autowired
    private VersementServiceIn versementService;
    
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		Utilisateur utilisateur1 = new Utilisateur();
		utilisateur1.setUsername("SalimaEl");
        utilisateur1.setLastname("elhammiouy");
        utilisateur1.setFirstname("Salima");
        utilisateur1.setGender(Gender.F);
        
        
        utilisateurService.saveUtilisateur(utilisateur1);


        Utilisateur utilisateur2 = new Utilisateur();
        utilisateur2.setUsername("ZakariaEl");
        utilisateur2.setLastname("elhammiouy");
        utilisateur2.setFirstname("Zakaria");
        utilisateur2.setGender(Gender.H);

        utilisateurService.saveUtilisateur(utilisateur2);

        Compte compte1 = new Compte();
        compte1.setNrCompte("11100000000110A");
        compte1.setRib("123345567892200348100182");
        compte1.setSolde(new BigDecimal(500000));
        compte1.setUtilisateur(utilisateur1);
        compte1.setDateCreation(new Date());

        compteService.saveCompte(compte1);

        Compte compte2 = new Compte();
        compte2.setNrCompte("010000B025001000");
        compte2.setRib("123345567892200348100183");
        compte2.setSolde(new BigDecimal(40000));
        compte2.setUtilisateur(utilisateur2);
        compte2.setDateCreation(new Date());

        compteService.saveCompte(compte2);

        Virement virement = new Virement();
        virement.setMontantVirement(new BigDecimal(4000));
        virement.setCompteBeneficiaire(compte2);
        virement.setCompteEmetteur(compte1);
        virement.setDateOperation(new Date());
        virement.setMotifVirement("Virement de 4000 dh");



        VirementDto virementDto = VirementMapper.map(virement);

        try {
            virementService.saveVirementDto(virementDto);
        }catch(Exception e){

        }

        Versement versement = new Versement();
        versement.setMontantVirement(new BigDecimal(200));
        versement.setCompteBeneficiaire(compte1);
        versement.setDateOperation(new Date());

        VersementDto versementDto = VersementMapper.map(versement);
        try {
            versementService.saveVersementDto(versementDto);
        }catch(Exception e){

        }
    }
		
	

}


