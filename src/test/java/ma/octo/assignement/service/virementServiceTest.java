package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Virement;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.mapper.VirementMapper;

@SpringBootTest
public class virementServiceTest {
	@Autowired
    private VirementMapper mapper;

    @Mock
    private static VirementDto virementDto;
    
    @Mock
    private static Virement virement;


    @BeforeEach
    public void init() {
        virementDto = new VirementDto();
        virement = new Virement();
        Utilisateur utilisateur1 = new Utilisateur(114L, "ZakariaEl1", Gender.H, "elhammiouy1", "Zakaria1", null);
        Utilisateur utilisateur2 = new Utilisateur(115L, "ZakariaEl2", Gender.H, "elhammiouy2", "Zakaria2", null);
        final Compte CompteEmetteur = new Compte(17L, "0012344700001100","123456789090001110010121" ,new Date() ,new BigDecimal(500000), utilisateur1);
        final Compte CompteBeneficiaire = new Compte(18L, "0012344700001200","123456789090001110010322" ,new Date() ,new BigDecimal(60000), utilisateur2);
        final BigDecimal montantVirement = new BigDecimal(3000);
        final String motifVirement = "virement_motif";
        final Date dateVirement = new Date();

        virementDto.setCompteEmetteur(CompteEmetteur);
        virementDto.setCompteBeneficiaire(CompteBeneficiaire);
        virementDto.setMontantVirement(montantVirement);
        virementDto.setMotifVirement(motifVirement);
        virementDto.setDateOperation(dateVirement);

        virement.setCompteEmetteur(CompteEmetteur);
        virement.setCompteBeneficiaire(CompteBeneficiaire);
        virement.setMontantVirement(montantVirement);
        virement.setMotifVirement(motifVirement);
        virement.setDateOperation(dateVirement);
    }

    @Test
    void mapVirementDtoToVirementTest(){
 
        Assertions.assertAll(() -> {

            Virement result = mapper.map(virementDto);

            Assertions.assertEquals(result.getCompteEmetteur().getNrCompte(), virementDto.getCompteEmetteur().getNrCompte());
            Assertions.assertEquals(result.getCompteBeneficiaire().getNrCompte(), virementDto.getCompteBeneficiaire().getNrCompte());
            Assertions.assertEquals(result.getMontantVirement(), virementDto.getMontantVirement());
            Assertions.assertEquals(result.getMotifVirement(), virementDto.getMotifVirement());
            Assertions.assertEquals(result.getDateOperation(), virementDto.getDateOperation());
        });
    }


    @Test
    void mapVirementToVirementDtoTest(){
        Assertions.assertAll(() -> {

            VirementDto result = mapper.map(virement);

            Assertions.assertEquals(result.getCompteEmetteur().getNrCompte(), virement.getCompteEmetteur().getNrCompte());
            Assertions.assertEquals(result.getCompteBeneficiaire().getNrCompte(), virement.getCompteBeneficiaire().getNrCompte());
            Assertions.assertEquals(result.getMontantVirement(), virement.getMontantVirement());
            Assertions.assertEquals(result.getMotifVirement(), virement.getMotifVirement());
            Assertions.assertEquals(result.getDateOperation(), virement.getDateOperation());
        });
    }
}
