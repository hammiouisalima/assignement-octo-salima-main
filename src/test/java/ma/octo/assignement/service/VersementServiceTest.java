package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Spy;
import org.springframework.boot.test.context.SpringBootTest;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.domain.util.Gender;
import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.service.impl.VersementService;

@SpringBootTest
public class VersementServiceTest {
	private VersementMapper mapper;

    @Spy
    private static VersementDto versementDto;
    @Spy
    private static Versement versement;

    private VersementService versementService;
    
    @BeforeEach
    public void init() {
        versementDto = new VersementDto();
        versement = new Versement();
        Utilisateur utilisateur1 = new Utilisateur(111L, "salimaaEl1", Gender.F, "Elhammiouy1", "salima1", null);

        final Compte CompteBeneficiaire = new Compte(131L, "0012344700001100","123456789090001110010121" ,new Date() ,new BigDecimal(500000), utilisateur1);
        final BigDecimal montantVersement = new BigDecimal(2000);
        final Date dateVersement = new Date();

        versementDto.setCompteBeneficiaire(CompteBeneficiaire);
        versementDto.setMontantV(montantVersement);
        versementDto.setDateO(dateVersement);


        versement.setCompteBeneficiaire(CompteBeneficiaire);
        versement.setMontantVirement(montantVersement);
        versement.setDateOperation(dateVersement);
    }


    @Test
    void mapVersementDtoToVersementTest(){
        Assertions.assertAll(() -> {

            Versement result = mapper.map(versementDto);


            Assertions.assertEquals(result.getCompteBeneficiaire().getRib(), versementDto.getCompteBeneficiaire().getRib());
            Assertions.assertEquals(result.getMontantVirement(), versementDto.getMontantV());
            Assertions.assertEquals(result.getDateOperation(), versementDto.getDateO());
        });
    }

    @Test
    void mapVersementToVersementDtoTest(){

        Assertions.assertAll(() -> {

            VersementDto result = mapper.map(versement);


            Assertions.assertEquals(result.getCompteBeneficiaire().getRib(), versement.getCompteBeneficiaire().getRib());
            Assertions.assertEquals(result.getMontantV(), versement.getMontantVirement());
            Assertions.assertEquals(result.getDateO(), versement.getDateOperation());
        });
    }
    
    
}

